const express = require('express');
const expressVue = require('express-vue');
const path = require('path');

/**
 * Express setup
 */
const app = express();

/**
 * ExpressVue setup
 */
const vueOptions = {
    rootPath: path.join(__dirname, 'vue')
};
const expressVueMiddleware = expressVue.init(vueOptions);
app.use(expressVueMiddleware);

/**
 * Routes
 */
app.get('/', (req, res) => {
    const data = {
        title: 'Hello World',
    };
    req.vueOptions = {
        head: {
            title: 'Hello World Page',
        },
    };
    res.statusCode = 200;
    res.renderVue('home.vue', data, req.vueOptions);
});

/**
 * Starts listening
 */
const port = 8080;
app.listen(port, () => {
    console.log('Listening on port ' + port);
});